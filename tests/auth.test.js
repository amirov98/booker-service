const request = require('supertest')
const app = require('../')
const User = require('../model/User')

jest.setTimeout(15000);

let token;
let id;

beforeAll((done) => {
  request(app)
    .post('/api/user/login')
    .send({
      email: 'arifamirov98@gmail.com',
      password: 'test9876',
    })
    .end((err, response) => {
      token = response.body.token; // save the token!
      done();
    });
});


afterAll(async () => {
  await User.findOneAndDelete({ email: 'test@test.com' })

  await request(app).delete(`/api/user/deleteEvent/${id}`).set('Authorization', `Bearer ${token}`);

})

describe('Register', () => {1
  const randomNumber = Math.floor(Math.random() * 1000);
  it('Register with correct credentials', async () => {
    const res = await request(app).post('/api/user/register').send({
      email: randomNumber + '@arif.com',
      password: 'test9876',
    }, 15000)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('user')
  })

  it('Register without required field', async () => {
    const res = await request(app).post('/api/user/register').send({
      email: 'arif@arif.com',
    }, 15000)
    expect(res.statusCode).toEqual(400)
  })

  it('Register with existing credentials', async () => {
    const res = await request(app).post('/api/user/register').send({
      email: 'arif@arif.com',
      password: 'test9876',
    }, 15000)
    expect(res.statusCode).toEqual(409)
  })
})

describe('Login', () => {
  it('Login with corect credentials', async () => {
    const res = await request(app).post('/api/user/login').send({
      email: 'arif@arif.com',
      password: 'test9876'
    }, 15000)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('token')
  })

  it('Login with incorrect credentials', async () => {
    const res = await request(app).post('/api/user/login').send({
      email: 'wrong@arif.com',
      password: 'test9876'
    }, 15000)
    expect(res.statusCode).toEqual(400)
  })

  it('Login without required field', async () => {
    const res = await request(app).post('/api/user/login').send({
      email: 'arif@arif.com'
    }, 15000)
    expect(res.statusCode).toEqual(400)
  })
})

describe('AddEvent', () => {
  it('Add event in the empty time slot', async () => {
    const res = await request(app).post('/api/user/addEvent').set('Authorization', `Bearer ${token}`)
    .send({
      location: 'room1',
      team: 'LAKERS',
      members: ['Arif'],
      startDateAndTime: '2020-08-30T16:15:00',
      endDateAndTime: '2020-08-30T16:20:00'
    }, 15000)
    id = res.body.event;
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('event');
  })

  it('Add event to a taken time slot', async () => {
    const res = await request(app).post('/api/user/addEvent').set('Authorization', `Bearer ${token}`)
    .send({
      location: 'room1',
      team: 'LAKERS',
      members: ['Arif'],
      startDateAndTime: '2020-09-28T22:00:00',
      endDateAndTime: '2020-09-28T22:01:00'
    }, 15000)
    expect(res.statusCode).toEqual(409);
  })
})

describe('GetRoom1', () => {
  it('Get events if they exist', async () => {
    const res = await request(app).get('/api/user/getEvents/room1').set('Authorization', `Bearer ${token}`)
    .send({
      location: 'room1'
    }, 15000)
    expect(res.statusCode).toEqual(200);
  })
})

describe('Get events by name', () => {
  it('Get events if they exist', async () => {
    const res = await request(app).get('/api/user/getEvents/member/Arif').set('Authorization', `Bearer ${token}`)
    expect(res.statusCode).toEqual(200);
  })

  it('Get events if there are no events for that name', async () => {
    const res = await request(app).get('/api/user/getEvents/member/Famil').set('Authorization', `Bearer ${token}`)
    expect(res.statusCode).toEqual(204);
  })
})

describe('Get event by team name', () => {
  it('Get events if they exist', async () => {
    const res = await request(app).get('/api/user/getEvents/team/LAKERS').set('Authorization', `Bearer ${token}`)
    expect(res.statusCode).toEqual(200);
  })

  it('Get events if there are no events for that team', async () => {
    const res = await request(app).get('/api/user/getEvents/team/TEST').set('Authorization', `Bearer ${token}`)
    expect(res.statusCode).toEqual(204);
  })
})