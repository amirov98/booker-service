// const http = require('http');
const express = require('express');
const app = express();
// const server = http.createServer(app);
const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

// Import Routes
const authRoute = require('./routes/auth');

// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers',
    'Origin, X-Requested-With, content-type, Accept, Authorization');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

// connect to DB
mongoose.connect(
  process.env.DB_CONNECT  ,
  { 
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  },
  () => {
    console.log('connected to DB!')
  }
);

// Middleware
app.use(express.json());


// Route Middlewares
app.use('/api/user', authRoute);

// console.log('soxush');

app.listen(3000, () => { console.log('hi from the server')});

module.exports = app;