const router = require('express').Router();
const User = require('../model/User');
const Event = require('../model/Event');
const Team = require('../model/Team');
const bcrypt = require('bcryptjs');
const {
  registerValidation,
  loginValidation,
  eventValidation,
  teamValidation
} = require('../validation');
const jwt = require('jsonwebtoken');
const checkAuth = require('../middleware/check-auth');

const fs = require('fs');
const {google} = require('googleapis');

const SCOPES = ['https://www.googleapis.com/auth/calendar'];
const TOKEN_PATH = 'token.json';

let oAuth2Client;

router.post('/register', async (req, res) => {
  // Validate the data
  const { error } = registerValidation(req.body);
  if(error) return res.status(400).send(error.details[0].message);

  // Checking for duplicate entry
  const userExists = 
    await User.findOne({ email: req.body.email })
  if (userExists) {
    return res.status(409).send('Email already exists')
  }

  // Hash the password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);


  // create a new user
  const user = new User({
    email: req.body.email,
    password: hashedPassword
  });
  try {
    const savedUser = await user.save();
    res.send({user: savedUser._id});
    console.log('info sent');
  } catch(err) {
    res.status(400).send(err);
  }
});


router.post('/login', async(req, res) => {
  console.log('login');
   // Validate the data
   const { error } = loginValidation(req.body);
   if (error) return res.status(400).send(error.details[0].message);

   // Checking if user exists
  const user = 
  await User.findOne({ email: req.body.email })
  if (!user) {
    return res.status(400).send('Email is not found');
  }

  // Password is correct
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if(!validPass) return res.status(400).send('Invalid password');

  // create and assign a token 
  const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET, {expiresIn: '1h'});
  console.log('login');
  // res.status(200).header(token).send(token);
  res.status(200).json({
    token: token,
    expiresIn: 3600
  });
});

router.post('/addEvent', checkAuth, async (req, res, next) => {
  // Validate the data
  const { error } = eventValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // check the time slot for availability
  const timeSlotEvents = await Event.find(
    {
      location: req.body.location,
      $or:[
        {
          startDateAndTime: {"$lte": req.body.startDateAndTime},
          endDateAndTime: {"$gt": req.body.startDateAndTime}
        },
        {
          startDateAndTime: {"$lt": req.body.endDateAndTime},
          endDateAndTime: {"$gte": req.body.endDateAndTime}
        },
        {
          startDateAndTime: {"$gte": req.body.startDateAndTime},
          endDateAndTime: {"$lte": req.body.endDateAndTime}
        }
      ]
    }
  );
  if (timeSlotEvents.length > 0) {
    console.log('timeslot taken');
    return res.status(409).end();
  }

  // create a new event
  const event = new Event ({
    location: req.body.location,
    team: req.body.team,
    members: req.body.members,
    startDateAndTime: req.body.startDateAndTime,
    endDateAndTime: req.body.endDateAndTime
  })

  try {
    const savedEvent = await event.save();
    res.status(200).send({event: savedEvent._id});
  } catch(err) {
    console.log('not added');
    return res.status(400).send(err);
  }
});

router.post('/addTeam', async (req, res, next) => {
  // Validate the data
  const { error } = teamValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // check if team name exists
  const existingTeam = await Team.find(
    {
      name: req.body.name
    }
  );
  if (existingTeam.length > 0) {
    return res.status(409).end();
  }

  // create a new team
  const team = new Team ({
    name: req.body.name,
    members: req.body.members
  })

  try {
    const savedTeam = await team.save();
    res.send({team: savedTeam._id});
  } catch(err) {
    res.status(400).send(err);
  }
});

router.delete('/deleteEvent/:id', checkAuth, (req, res, next) => {
  const eventId = req.params.id
  console.log(eventId);
  
  Event.findByIdAndRemove(eventId, (err, doc) => {
    if(err || !doc) {
      res.status(204).end();
    } else {
      res.status(200).end();
      console.log('removed event');
    }})
    .catch(err => console.log(err));
})

router.get('/getEvents', checkAuth, async (req, res) => {
  const events = await Event.find().sort({ startDateAndTime: 1 });
  // console.log(events);
  if (events.length > 0) {
    console.log("from the if");
    res.status(200).json({
      events
    });
  } else {
    console.log('in else');
    res.status(204).send('No content');
  }
})

router.get('/getEvents/room1', checkAuth, async (req, res) => {
  const events = await Event.find({
     location: 'room1'
    }).sort({
       startDateAndTime: 1 
      });
  console.log(events);
  if (events.length > 0) {
    res.status(200).json({
      events
    });
  } else {
    console.log('in else');
    res.status(204).send('No content');
  }
})

router.get('/getEvents/room2', checkAuth, async (req, res) => {
  const events = await Event.find({
     location: 'room2'
    }).sort({
       startDateAndTime: 1 
      });
  console.log(events);
  if (events.length > 0) {
    res.status(200).json({
      events
    });
  } else {
    console.log('in else');
    res.status(204).send('No content');
  }
})

router.get('/getEvents/room3', checkAuth, async (req, res) => {
  const events = await Event.find({
     location: 'room3'
    }).sort({
       startDateAndTime: 1 
      });
  console.log(events);
  if (events.length > 0) {
    res.status(200).json({
      events
    });
  } else {
    console.log('in else');
    res.status(204).send('No content');
  }
})

router.get('/getEvents/member/:name', checkAuth, async(req, res) => {
  const name = req.params.name;
  console.log(req.params.name);
  const teams = await Team.find({
    members: name
  });
  console.log(teams);
  const events = await Event.find({
     $or:
      [{
        members: name
      },
      {
        team: { $in: teams[0].name }
      }]
   }).sort({
      startDateAndTime: 1 
     });
 console.log(events);
 if (events.length > 0) {
   res.status(200).json({
     events
   });
 } else {
   console.log('in else');
   res.status(204).send('No content');
 }
})

router.get('/getEvents/team/:team', checkAuth, async(req, res) => {
  const team = req.params.team;
  const events = await Event.find({
     team
   }).sort({
      startDateAndTime: 1 
     });
 console.log(events);
 if (events.length > 0) {
   res.status(200).json({
     events
   });
 } else {
   console.log('in else');
   res.status(204).send('No content');
 }
})

router.get('/auth/google', (req, res, next) => {
  fs.readFile('credentials.json', (err, content) => {
    console.log("REACHED ENDPINT");
    if (err) return console.log('Error loading client secret file:', err);
    
    const credentials = JSON.parse(content);
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) {
      const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
      });
      console.log('Authorize this app by visiting this url:', authUrl);
      res.status(200).json({
        auth: authUrl});
      // callback
      // listEvents(oAuth2Client);
    } else {
      oAuth2Client.setCredentials(JSON.parse(token));
      res.status(200).send("Success");
      // listEvents(oAuth2Client);
    }
    });
  });
})

router.get('/auth/google/redirect', (req, res) => {
  console.log('REDIRECTED')
  console.log(req.query.code)

  const accessCode = req.query.code;
  oAuth2Client.getToken(accessCode, (err, token) => {
    if (err) {
      console.error('Error retrieving access token', err);
    } else {
      oAuth2Client.setCredentials(token);
  
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      res.redirect("success");
      // callback(oAuth2Client);
    }
  });
});

router.get('/auth/google/success', (req, res) => {
  res.status(200).send('Successfully logged in! Press the done button to follow.');
})

router.get('/auth/google/session', (req, res) => {
  console.log('from success');
  fs.readFile(TOKEN_PATH, (err, token) => {
    console.log('from readFile');
    if (err) {
      res.status(401).end();
    } else {
      res.status(200).end();
    }
    });
})

router.get('/auth/calendar/events', (req, res) => {
  console.log('hi from calendar');
  const calendar = google.calendar({version: 'v3', oAuth2Client});
  console.log(oAuth2Client);
  calendar.events.list({
    calendarId: 'primary',
    timeMin: (new Date()).toISOString(),
    maxResults: 10,
    singleEvents: true,
    orderBy: 'startTime',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    const events = res.data.items;
    if (events.length) {
      res.status(200).json({
        events: events
      })
      console.log('Upcoming 10 events:');
      events.map((event, i) => {
        const start = event.start.dateTime || event.start.date;
        console.log(`${start} - ${event.summary}`);
      });
    } else {
      console.log('No upcoming events found.');
    }
  });
})


module.exports = router;