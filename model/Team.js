const mongoose = require('mongoose');

const teamSchema = new mongoose.Schema({
  name: {
    type: String,
    enum: [ 'LAKERS', 'BULLS', 'NONE', 'TEST', 'THESIS' ],
    required: false
  },
  members: {
    type: [String],
    required: true
  }
})

module.exports = mongoose.model('Team', teamSchema);