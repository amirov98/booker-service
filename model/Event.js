const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema({
  location: {
    type: String,
    enum: ['room1', 'room2', 'room3'],
    required: true
  },
  team: {
    type: String,
    enum: [ 'LAKERS', 'BULLS', 'NONE', 'TEST', 'THESIS' ],
    required: false
  },
  members: {
    type: [String],
    required: false
  },
  startDateAndTime: {
    type: Date,
    required: true
  },
  endDateAndTime: {
    type: Date,
    required: true
  }
})

module.exports = mongoose.model('Event', eventSchema);