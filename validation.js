// validation
const Joi = require('@hapi/joi');

// Register validation
const registerValidation = (data) => {
  const schema = 
    Joi.object({
      email: Joi.string().
        email().
        required(),
      password: Joi.string().
        min(6).
        required()
  });
  return schema.validate(data);
}

// Login validation
const loginValidation = data => {
  const schema = 
    Joi.object({
      email: Joi.string().
        email().
        required(),
      password: Joi.string().
        min(6).
        required()
  });
  return schema.validate(data);
}

// Event validation
const eventValidation = data => {
  const schema = 
  Joi.object({
    location: Joi.string().
      required(),
    team: Joi.string(),
    members: Joi.array().items(),
    startDateAndTime: Joi.date().required(),
    endDateAndTime: Joi.date().required()
  });
  return schema.validate(data);
}

// Team validation
const teamValidation = data => {
  const schema = 
  Joi.object({
    name: Joi.string().required(),
    members: Joi.array().items(Joi.string())
  });
  return schema.validate(data);
}

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.eventValidation = eventValidation;
module.exports.teamValidation = teamValidation;